# Mermaid Examples

_Pre-requisite_: Download the **Mermaid** plugin for IntelliJ. [Instructions](https://www.jetbrains.com/guide/go/tips/mermaid-js-support-in-markdown/)

## Sequence Diagrams
* [prd-init-var-thsd](https://gitlab.com/heb-engineering/teams/enterprise/solutions/inventory/mia/mia-core/mia-tasks/prd-init-var-thsd/-/tree/add-documentation)

## Flowcharts
* [inventory-tracker-api](https://gitlab.com/heb-engineering/teams/enterprise/solutions/inventory/tim-core/backend/api/inventory-tracker-api/-/tree/adding-mermaid)
* [tim-error-message-handler](https://gitlab.com/heb-engineering/teams/enterprise/solutions/inventory/common-frameworks/error-ticketing/tim-error-message-handler/-/tree/update-documentation)
* [inven-rcon-job](https://gitlab.com/heb-engineering/teams/enterprise/solutions/inventory/tim-valuation/warehouse/backend/task-scheduler/jobs/core/inven-rcon-job/-/tree/add-documentation)

## References
* https://mermaid.js.org/syntax/sequenceDiagram.html
* https://mermaid.js.org/syntax/flowchart.html
* https://mermaid.js.org/syntax/examples.html